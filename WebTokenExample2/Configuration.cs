﻿using System;

namespace WebTokenExample2
{
	public static class ConfigurationHelper
	{
		public static Configuration CheckConfiguration(this Configuration config)
		{
			_ = config.AuthorizationEndpoint ?? throw new ArgumentNullException("AuthorizationEndpoint");
			_ = config.ClientId ?? throw new ArgumentNullException("ClientId");
			_ = config.ClientSecret ?? throw new ArgumentNullException("ClientSecret");
			_ = config.CallbackPath ?? throw new ArgumentNullException("CallbackPath");
			_ = config.TokenEndpoint ?? throw new ArgumentNullException("TokenEndpoint");
			return config;
		}
	}

	public class Configuration
	{
		public string ClientId { get; set; } = null!;
		public string ClientSecret { get; set; } = null!;
		public string CallbackPath { get; set; } = null!;
		public string AuthorizationEndpoint { get; set; } = null!;
		public string TokenEndpoint { get; set; } = null!;
	}
}
