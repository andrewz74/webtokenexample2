﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebTokenExample2.Pages
{
    [Authorize()]
	public class PrivateModel : PageModel
	{
		public void OnGet()
		{
			OpenIdTokenString = HttpContext.GetTokenAsync("id_token") ?? throw new ArgumentNullException("id_token is null");
			AccessTokenString = HttpContext.GetTokenAsync("access_token") ?? throw new ArgumentNullException("access_token is null");
			PhotoProfile = HttpContext.User.Claims.Where(t => t.Type == "picture").Select(t => t.Value).FirstOrDefault();
			Email = HttpContext.User.Claims.Where(t => t.Type == ClaimTypes.Email).Select(t => t.Value).FirstOrDefault();
			NickName = HttpContext.User.Claims.Where(t => t.Type == "nickname").Select(t => t.Value).FirstOrDefault();
		}

		public Task<string> OpenIdTokenString { get; private set; } = null!;
		public Task<string> AccessTokenString { get; private set; } = null!;
		public string? PhotoProfile { get; private set; }
        public string? Email { get; private set; }
        public string? NickName { get; private set; }
    }
}
