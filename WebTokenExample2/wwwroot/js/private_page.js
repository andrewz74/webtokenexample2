﻿$(function () {
	var responseP = $("#response");
	var statusP = $("#statusValue");
	var tokenP = $("#AccessTokenTxt");

	function clearInfo() {
		responseP.text('');
		statusP.text('');
	}

	function linkClick1() {
		clearInfo();
		$.ajax({
			url: "http://localhost:43202/api/public",
			success: function (data, status) {
				statusP.text(status);
				responseP.text(JSON.stringify(data));
			},
			error: function (err) {
				statusP.text(err.status);
				responseP.text(err.statusText);
			}
		});
	}

	function linkClick2() {
		clearInfo();
		var tokenString = tokenP.val();
		$.ajax({
			url: "http://localhost:43202/api/private",
			headers: { "Authorization": "Bearer " + tokenString },
			success: function (data, status) {
				statusP.text(status);
				responseP.text(JSON.stringify(data));
			},
			error: function (err) {
				statusP.text(err.status);
				responseP.text(err.statusText);
			}
		});
	}

	$("#bt1").click(linkClick1);
	$("#bt2").click(linkClick2);
});