using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace WebTokenExample2
{
    public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			IdentityModelEventSource.ShowPII = true; // Show extra debug info
			var configuration = Configuration.GetSection("Configuration").Get<Configuration>().CheckConfiguration();

			services.AddAuthentication(config =>
			{
				// We check the cookie to confirm that we are authenticated
				config.DefaultAuthenticateScheme = "ClientCookie";
				// When we sign in we will deal out a cookie
				config.DefaultSignInScheme = "ClientCookie";
				// use this to check if we are allowed to do something.
				config.DefaultChallengeScheme = "OurServer";
			})
				.AddCookie("ClientCookie")
				.AddOpenIdConnect("OurServer", options => {
					options.RequireHttpsMetadata = false;
					// Set the authority to your Auth0 domain
					options.Authority = configuration.AuthorizationEndpoint;

					// Configure the Auth0 Client ID and Client Secret
					options.ClientId = configuration.ClientId;
					options.ClientSecret = configuration.ClientSecret;
					//options.RequireHttpsMetadata = false;

					// Set response type to code
					options.ResponseType = OpenIdConnectResponseType.Code;

					// Configure the scope
					options.Scope.Clear();
					options.Scope.Add("openid");
					options.GetClaimsFromUserInfoEndpoint = true;

					options.CallbackPath = configuration.CallbackPath;

					// Configure the Claims Issuer to be Auth0
					options.ClaimsIssuer = "Auth0";
					options.UsePkce = false;
					// Skip check on Nonce or State string
					/*options.ProtocolValidator = new OpenIdConnectProtocolValidator
					{
						RequireNonce = false,
						RequireState = false
					};*/

					options.SaveTokens = true; // <- Save tokens
				});

			services.AddSingleton(configuration);
			services.AddHttpClient();
			services.AddControllersWithViews();
			services.AddRazorPages();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
            IdentityModelEventSource.ShowPII = true; // <- show extra information in error
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
			}

			app.UseCookiePolicy(new CookiePolicyOptions()
			{
				HttpOnly = HttpOnlyPolicy.Always,
				Secure = CookieSecurePolicy.Always,
				MinimumSameSitePolicy = SameSiteMode.None
			});

			app.UseStaticFiles();
			app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapRazorPages();
			});
		}
	}
}
