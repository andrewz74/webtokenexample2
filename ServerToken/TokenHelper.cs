﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;

namespace ServerToken
{
	public interface ITokenHelper
	{
		string GenerateOpenIdToken(in CertificateInfo ci, in string clientId, in string username, in string nonce);
		string GenerateAccessToken(in CertificateInfo ci, in string audience);
	}

	public class TokenHelper : ITokenHelper
	{
		private readonly Configuration _configuration;

		private TokenHelper() => throw new NotImplementedException();
		public TokenHelper(Configuration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		public string GenerateOpenIdToken(in CertificateInfo ci, in string clientId, in string username, in string nonce)
		{
			var certificate = new X509Certificate2(ci.Pfx, _configuration.PfxPassword);
			var securityKey = new X509SecurityKey(certificate);

			var claims = new[]
			{
				new Claim(JwtRegisteredClaimNames.Email, $"{username}@exampledomain.com"),
				new Claim("nickname", username),
				new Claim("iat", DateTimeOffset.Now.ToUnixTimeSeconds().ToString()),
				new Claim("sub", "az"),
				new Claim("picture", _configuration.Issuer + "images/omino.jpg"),
				new Claim("nonce", nonce ?? string.Empty)
			};

			var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.RsaSha256);
			var token = new JwtSecurityToken(
				_configuration.Issuer,
				clientId, // <- Audience
				claims,
				notBefore: DateTime.Now,
				expires: DateTime.Now.AddMinutes(5),
				signingCredentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		public string GenerateAccessToken(in CertificateInfo ci, in string audience)
		{
			var certificate = new X509Certificate2(ci.Pfx, _configuration.PfxPassword);
			var securityKey = new X509SecurityKey(certificate);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, "xa"),
				new Claim("scope", "openid profile read:messages"), // <- add custom API permission
				new Claim("iat", DateTimeOffset.Now.ToUnixTimeSeconds().ToString()),
				new Claim("sub", "az"),
				// new Claim("aud", audience)
				//, new Claim("aud", "another audiance") // <- Accept multiple audiance
			};

			var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.RsaSha256);
			var token = new JwtSecurityToken(
				_configuration.Issuer,
				audience, // <- Accept only one audiance
				claims,
				notBefore: DateTime.Now,
				expires: DateTime.Now.AddMinutes(5),
				signingCredentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
	}
}
