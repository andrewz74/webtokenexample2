using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ServerToken
{
    public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			var configuration = Configuration.GetSection("Configuration").Get<Configuration>().UpdateConfiguration();

			services.AddCors(options =>
			{
				options.AddPolicy(name: "mycors", builder => builder
					.AllowAnyMethod()
					.AllowAnyOrigin()
					.AllowAnyHeader()
				);
			});

			services.AddSingleton(configuration);
			services.AddSingleton<IClientConfiguration, ClientConfiguration>();
			services.AddSingleton<ICertificateHelper, CertificateHelper>();
			services.AddSingleton<ITokenHelper, TokenHelper>();
			services.AddSingleton<IHelper, Helper>();

			services.AddRazorPages();
			services.AddMvc(c => c.EnableEndpointRouting = false);
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
			}

			app.UseCookiePolicy(new CookiePolicyOptions()
			{
				HttpOnly = HttpOnlyPolicy.Always,
				Secure = CookieSecurePolicy.Always,
				MinimumSameSitePolicy = SameSiteMode.None
			});

			app.UseStaticFiles();
			app.UseRouting();
			app.UseCors();

			app.UseMvcWithDefaultRoute();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapRazorPages();
			});
		}
	}
}
