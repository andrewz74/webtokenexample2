﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ServerToken
{
    public interface IClientConfiguration
    {
        bool CheckConfigurationForLogin(string clientId, string callBack, string callUrl);
        bool CheckConfigurationForToken(string clientId, string callUrl);
        bool CheckValidReturnCode(string clientId, string returnCode);
        bool CheckTokenClientId(string clientId, string redirect_uri);
        string GetReturnCode(string clientId, string username, string nonce);
        string GetNonceFromCode(string code);
        string GetUsernameFromCode(string code);
        string GetAudianceFromClientId(string clientId);
    }

    public class ClientConfiguration : IClientConfiguration
    {
        private readonly Configuration _configuration;
        private readonly List<ReturnCodeValue> _codeValueCollection = new List<ReturnCodeValue>();
        private readonly ReaderWriterLockSlim _locked = new ReaderWriterLockSlim();

        private ClientConfiguration() => throw new NotSupportedException();
        public ClientConfiguration(Configuration configuration)
        {
            _configuration = configuration;
        }

        public bool CheckConfigurationForLogin(string clientId, string callBack, string callUrl)
        {
            return _configuration.Clients.Any(t => t.CallBack == callBack && t.CallUrl == callUrl && t.ClientId == clientId);
        }

        public bool CheckConfigurationForToken(string clientId, string callUrl)
        {
            return _configuration.Clients.Any(t => t.CallBack == callUrl && t.ClientId == clientId);
        }

        public string GetAudianceFromClientId(string clientId)
        {
            return _configuration.Clients.Where(t => t.ClientId == clientId).Select(t => t.Audience).FirstOrDefault();
        }

        public string GetReturnCode(string clientId, string username, string nonce)
        {
            _locked.EnterWriteLock();
            try
            {
                // Check if there is some expired item and delete them
                var toDelete = _codeValueCollection.Where(t => t.Expired < DateTime.Now).ToList();
                toDelete.ForEach(t => _codeValueCollection.Remove(t));

                string guid = Guid.NewGuid().ToString().Replace("-", string.Empty);
                _codeValueCollection.Add(new ReturnCodeValue(
                    username: username,
                    clientId: clientId,
                    code: guid,
                    expired: DateTime.Now.AddMinutes(5),
                    codeAlreadyUsed: false,
                    nonce: nonce
                ));

                return guid;
            }
            finally
            {
                _locked.ExitWriteLock();
            }
        }

        public bool CheckValidReturnCode(string clientId, string returnCode)
        {
            _locked.EnterUpgradeableReadLock();
            try
            {
                var item = _codeValueCollection.Where(t => t.ClientId == clientId && t.Code == returnCode).FirstOrDefault();
                if (item == null)
                {
                    return false;
                }

                _locked.EnterWriteLock();
                try
                {
                    if (item.Expired > DateTime.Now && item.CodeAlreadyUsed == false)
                    {
                        item.CodeAlreadyUsed = true;
                        return true;
                    }

                    _codeValueCollection.Remove(item);
                    return false;
                }
                finally
                {
                    _locked.ExitWriteLock();
                }
            }
            finally
            {
                _locked.ExitUpgradeableReadLock();
            }
        }

        public bool CheckTokenClientId(string clientId, string redirect_uri)
        {
            return CheckConfigurationForToken(clientId, redirect_uri);
        }

        public string GetNonceFromCode(string code)
        {
            _locked.EnterReadLock();
            try
            {
                return _codeValueCollection.Where(t => t.Code == code).Select(t => t.Nonce).FirstOrDefault();
            }
            finally
            {
                _locked.ExitReadLock();
            }
        }

        public string GetUsernameFromCode(string code)
        {
            _locked.EnterReadLock();
            try
            {
                return _codeValueCollection.Where(t => t.Code == code).Select(t => t.Username).FirstOrDefault();
            }
            finally
            {
                _locked.ExitReadLock();
            }
        }
    }

    public class ReturnCodeValue
    {
        public ReturnCodeValue(string username, string code, string clientId, DateTime expired, bool codeAlreadyUsed, string nonce)
        {
            Username = username;
            Code = code;
            ClientId = clientId;
            Expired = expired;
            CodeAlreadyUsed = codeAlreadyUsed;
            Nonce = nonce;
        }

        public string Username { get; set; }
        public string Code { get; set; }
        public string ClientId { get; set; }
        public DateTime Expired { get; set; }
        public bool CodeAlreadyUsed { get; set; }
        public string Nonce { get; set; }
    }
}
