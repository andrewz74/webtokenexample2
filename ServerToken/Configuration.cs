﻿using System;

namespace ServerToken
{
	public static class ConfigurationHelper
    {
		public static Configuration UpdateConfiguration(this Configuration config)
		{
			config.Issuer = GetEnvironmentValue("Issuer", config.Issuer);
			config.AuthorizationEndpoint = GetEnvironmentValue("AuthorizationEndpoint", config.AuthorizationEndpoint);
			config.TokenEndpoint = GetEnvironmentValue("TokenEndpoint", config.TokenEndpoint);

			_ = config.AuthorizationEndpoint ?? throw new ArgumentNullException("AuthorizationEndpoint");
			_ = config.Clients ?? throw new ArgumentNullException("Clients");
			_ = config.FileNameCertificateInfo ?? throw new ArgumentNullException("FileNameCertificateInfo");
			_ = config.Issuer ?? throw new ArgumentNullException("Issuer");
			_ = config.PathData ?? throw new ArgumentNullException("PathData");
			_ = config.PfxPassword ?? throw new ArgumentNullException("PfxPassword");
			_ = config.RsaKeySize != int.MinValue ? 0 : throw new ArgumentNullException("RsaKeySize");
			_ = config.TokenEndpoint ?? throw new ArgumentNullException("TokenEndpoint");
			return config;
		}

		private static string GetEnvironmentValue(in string key, in string defaultValue)
        {
			var v = Environment.GetEnvironmentVariable(key);
			return string.IsNullOrEmpty(v) ? defaultValue : v;
        }
	}

	public class Configuration
	{
		public string PathData { get; set; } = null!;
		public string FileNameCertificateInfo { get; set; } = null!;
		public int RsaKeySize { get; set; } = int.MinValue;
		public string PfxPassword { get; set; } = null!;
		public string Issuer { get; set; } = null!;
		public string AuthorizationEndpoint { get; set; } = null!;
		public string TokenEndpoint { get; set; } = null!;
		public Client[] Clients { get; set; } = null!;
	}

	public class Client
	{
		public string ClientId { get; set; } = null!;
		public string ClientSecret { get; set; } = null!;
		public string CallBack { get; set; } = null!;
		public string CallUrl { get; set; } = null!;
		public string Audience { get; set; } = null!;
	}
}
