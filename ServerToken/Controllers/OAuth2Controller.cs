﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ServerToken.Controllers
{
	public class OAuth2Controller : Controller
	{
		private readonly IClientConfiguration _clientConfiguration;
		private readonly IHelper _helper;

		public OAuth2Controller(IClientConfiguration clientConfiguration, IHelper helper)
		{
			_clientConfiguration = clientConfiguration ?? throw new ArgumentNullException(nameof(clientConfiguration));
			_helper = helper ?? throw new ArgumentNullException(nameof(helper));
		}

		public async Task<IActionResult> Token(
			string grant_type, // flow of access_token request
			string code, // confirmation of the authentication process
			string redirect_uri,
			string client_id,
			string refresh_token)
		{
			if (grant_type != "authorization_code")
			{
				return StatusCode(400);
			}

			bool valid = _clientConfiguration.CheckValidReturnCode(client_id, code);
			if (valid == false)
			{
				return StatusCode(400);
			}

			// some mechanism for validating the code
			valid = _clientConfiguration.CheckTokenClientId(client_id, redirect_uri);
			if (valid == false)
			{
				return StatusCode(400);
			}

			string nonce = _clientConfiguration.GetNonceFromCode(code);
			if (string.IsNullOrEmpty(nonce))
			{
				return StatusCode(400);
			}

			string username = _clientConfiguration.GetUsernameFromCode(code) ?? "-";
			var audiance = _clientConfiguration.GetAudianceFromClientId(client_id);

			var openIdTokenTask = _helper.GetOpenIdTokenAsync(client_id, username, nonce);
			var accessTokenTask = _helper.GetAccessTokenAsync(audiance);

			var responseObject = new
			{
				id_token = await openIdTokenTask,
				access_token = await accessTokenTask,
				token_type = "Bearer",
				//refresh_token = "Token for refresh" // <- not implemented
			};

			return Ok(responseObject);
		}
	}
}
