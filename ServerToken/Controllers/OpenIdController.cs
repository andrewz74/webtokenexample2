﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json.Serialization;

namespace ServerToken.Controllers
{
	[Route(".well-known/openid-configuration")]
	[ApiController]
	[EnableCors("mycors")]
	public class OpenIdController : ControllerBase
	{
		private readonly Configuration _configuration;
		public OpenIdController(Configuration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}

		public IActionResult Get()
		{
			var obj = new OpenIdConfiguration
			{
				Issuer = _configuration.Issuer,
				AuthorizationEndpoint = _configuration.AuthorizationEndpoint,
				TokenEndpoint = _configuration.TokenEndpoint,
				DeviceAuthorizationEndpoint = null,
				UserinfoEndpoint = null,
				MfaChallengeEndpoint = null,
				JwksUri = _configuration.Issuer + ".well-known/jwks.json",
				RegistrationEndpoint = null,
				RevocationEndpoint = null,
				ScopesSupported = new[] { "openid" },
				ResponseTypesSupported = new[] { "code" },
				CodeChallengeMethodsSupported = new[] { "S256" },
				ResponseModesSupported = new[] { "form_post" },
				SubjectTypesSupported = new[] { "public" },
				IdTokenSigningAlgValuesSupported = new[] { "RS256" },
				TokenEndpointAuthMethodsSupported = new[] { "client_secret_post" },
				ClaimsSupported = new[] { "aud", "auth_time", "created_at", "email", "exp", "iat", "nickname", "sub", "picture" },
				RequestUriParameterSupported = false
			};

			return Ok(obj);
		}
	}

	public class OpenIdConfiguration
	{
		[JsonPropertyName("issuer")]
		public string Issuer { get; set; } = null!;

		[JsonPropertyName("authorization_endpoint")]
		public string AuthorizationEndpoint { get; set; } = null!;

		[JsonPropertyName("token_endpoint")]
		public string TokenEndpoint { get; set; } = null!;

		[JsonPropertyName("device_authorization_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string[]? DeviceAuthorizationEndpoint { get; set; }

		[JsonPropertyName("userinfo_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string? UserinfoEndpoint { get; set; }

		[JsonPropertyName("mfa_challenge_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string? MfaChallengeEndpoint { get; set; }

		[JsonPropertyName("jwks_uri")]
		public string JwksUri { get; set; } = null!;

		[JsonPropertyName("registration_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string? RegistrationEndpoint { get; set; }

		[JsonPropertyName("revocation_endpoint"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public string? RevocationEndpoint { get; set; }

		[JsonPropertyName("scopes_supported")]
		public string[] ScopesSupported { get; set; } = null!;

		[JsonPropertyName("response_types_supported")]
		public string[] ResponseTypesSupported { get; set; } = null!;

		[JsonPropertyName("code_challenge_methods_supported")]
		public string[] CodeChallengeMethodsSupported { get; set; } = null!;

		[JsonPropertyName("response_modes_supported")]
		public string[] ResponseModesSupported { get; set; } = null!;

		[JsonPropertyName("subject_types_supported")]
		public string[] SubjectTypesSupported { get; set; } = null!;

		[JsonPropertyName("id_token_signing_alg_values_supported")]
		public string[] IdTokenSigningAlgValuesSupported { get; set; } = null!;

		[JsonPropertyName("token_endpoint_auth_methods_supported")]
		public string[] TokenEndpointAuthMethodsSupported { get; set; } = null!;

		[JsonPropertyName("claims_supported")]
		public string[] ClaimsSupported { get; set; } = null!;

		[JsonPropertyName("request_uri_parameter_supported")]
		public bool RequestUriParameterSupported { get; set; }
	}
}
