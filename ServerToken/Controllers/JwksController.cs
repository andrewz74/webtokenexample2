﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ServerToken.Controllers
{
	[Route(".well-known/jwks.json")]
	[ApiController]
	[EnableCors("mycors")]
	public class JwksController : ControllerBase
	{
		private readonly IHelper _helper;

		public JwksController(IHelper helper)
		{
			_helper = helper ?? throw new ArgumentNullException(nameof(helper));
		}
		
		public async Task<IActionResult> GetAsync()
		{
			var certificateInfo = await _helper.GetCertificateInfoFromDiskAsync();

			var obj = new KeyJwtClass
			{
				Keys = new[]
				{
					new JwksClass
					{
						Alg = "RS256",
						Kty = "RSA",
						Use = "sig",
						N = certificateInfo.Modulus,
						E = "AQAB",
						Kid= certificateInfo.KeyId,
						X5t = certificateInfo.X5t,
						X5c = new [] { certificateInfo.PublicCertificateBase64 }
					}
				}
			};

			return Ok(obj);
		}
	}
	public class KeyJwtClass
	{
		[JsonPropertyName("keys"), JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
		public JwksClass[] Keys { get; set; } = null!;
	}

	public class JwksClass
	{
		[JsonPropertyName("alg")]
		public string Alg { get; set; } = null!;

		[JsonPropertyName("kty")]
		public string Kty { get; set; } = null!;

		[JsonPropertyName("use")]
		public string Use { get; set; } = null!;

		[JsonPropertyName("n")]
		public string N { get; set; } = null!;

		[JsonPropertyName("e")]
		public string E { get; set; } = null!;

		[JsonPropertyName("kid")]
		public string Kid { get; set; } = null!;

		[JsonPropertyName("x5t")]
		public string X5t { get; set; } = null!;

		[JsonPropertyName("x5c")]
		public string[] X5c { get; set; } = null!;
	}
}
