﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace ServerToken
{
	public interface ICertificateHelper
	{
		CertificateInfo GenerateCertificate();
	}

	public class CertificateHelper : ICertificateHelper
	{
		private readonly Configuration _configuration;

		private CertificateHelper() => throw new NotImplementedException();
		public CertificateHelper(Configuration configuration)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
		}
		public CertificateInfo GenerateCertificate()
		{
			var rsa = RSA.Create(_configuration.RsaKeySize); // generate asymmetric key pair

			var req = new CertificateRequest("cn=localhost", rsa, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
			var cert = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(1));
			var securityKey = new X509SecurityKey(cert);

			string modulus = WebEncoders.Base64UrlEncode(rsa.ExportParameters(false).Modulus);
			string x5t = securityKey.X5t;
			string keyId = securityKey.KeyId;
			byte[] pfx = cert.Export(X509ContentType.Pfx, _configuration.PfxPassword);
			string publicCertficateBase64 = Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.None);

			return new CertificateInfo(pfx, publicCertficateBase64, modulus, x5t, keyId);
		}
	}

	public class CertificateInfo
	{
        public CertificateInfo(in byte[] pfx, in string publicCertificateBase64, in string modulus, in string x5t, in string keyId)
        {
			Pfx = pfx;
			PublicCertificateBase64 = publicCertificateBase64;
			Modulus = modulus;
			X5t = x5t;
			KeyId = keyId;
        }

		public byte[] Pfx { get; }
		public string PublicCertificateBase64 { get; }
		public string Modulus { get; }
		public string X5t { get; }
		public string KeyId { get; }
	}
}
