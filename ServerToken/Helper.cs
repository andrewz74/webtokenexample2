﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ServerToken
{
	public interface IHelper
	{
		Task<CertificateInfo> GetCertificateInfoFromDiskAsync();
		Task<string> GetOpenIdTokenAsync(string clientId, string username, string nonce);
		Task<string> GetAccessTokenAsync(string clientId);
	}

	public class Helper : IHelper
	{
		private readonly Configuration _configuration;
		private readonly ICertificateHelper _certificateHelper;
		private readonly ITokenHelper _tokenHelper;

		private Helper() => throw new NotImplementedException();
		public Helper(Configuration configuration, ICertificateHelper certificateHelper, ITokenHelper tokenHelper)
		{
			_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
			_certificateHelper = certificateHelper ?? throw new ArgumentNullException(nameof(certificateHelper));
			_tokenHelper = tokenHelper ?? throw new ArgumentNullException(nameof(tokenHelper));
		}
		public async Task<CertificateInfo> GetCertificateInfoFromDiskAsync()
		{
			CertificateInfo certificate;
			string filename = Path.Combine(_configuration.PathData, _configuration.FileNameCertificateInfo);
			if (!File.Exists(filename))
			{
				// create new certificate
				certificate = _certificateHelper.GenerateCertificate();
				CheckDirectoryDestination(_configuration.PathData);
				var jsonString = JsonConvert.SerializeObject(certificate);
				await File.WriteAllTextAsync(filename, jsonString);
			}
			else
			{
				string jsonString = await File.ReadAllTextAsync(filename);
				certificate = JsonConvert.DeserializeObject<CertificateInfo>(jsonString) ?? throw new ArgumentNullException($"{_configuration.FileNameCertificateInfo} is not a valid JSON string");
			}

			return certificate;
		}

		public async Task<string> GetOpenIdTokenAsync(string clientId, string username, string nonce)
		{
			return _tokenHelper.GenerateOpenIdToken(await GetCertificateInfoFromDiskAsync(), clientId, username, nonce);
		}

		public async Task<string> GetAccessTokenAsync(string clientId)
		{
			return _tokenHelper.GenerateAccessToken(await GetCertificateInfoFromDiskAsync(), clientId);
		}

		private void CheckDirectoryDestination(in string pathData)
		{
			var di = new DirectoryInfo(pathData);
			if (!di.Exists)
			{
				di.Create();
			}
		}
	}
}
