using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ServerToken.Pages
{
    [IgnoreAntiforgeryToken(Order = 1001)]
    public class LoginModel : PageModel
    {
		private readonly IClientConfiguration _clientConfiguration;


        public LoginModel(IClientConfiguration clientConfiguration)
		{
			_clientConfiguration = clientConfiguration;
		}

        public void OnGet(string scope, string client_id, string response_type, string redirect_uri, string state, string nonce)
        {
            Username = string.Empty;
            Message = string.Empty;

            if (scope != "openid")
            {
                Message = "scope value is invalid";
                return;
            }

            if (response_type != "code")
            {
                Message = "Response_type value is invalid";
                return;
            }

            string referer = Request.Headers["Referer"];
            if (string.IsNullOrEmpty(referer))
            {
                Message = "Referer is null";
                return;
            }

            if (!_clientConfiguration.CheckConfigurationForLogin(client_id, redirect_uri, referer))
            {
                Message = "Parameter configuration is not recognized has valid";
            }
        }

        public void OnPost(string username, string client_id, string scope, string response_type, string redirect_uri, string state, string nonce)
        {
            Username = username;
            var codeString = _clientConfiguration.GetReturnCode(client_id, username, nonce);

            CodeString = codeString;
            StateString = state;
            RedirectUri = redirect_uri;
        }


        public string? Username { get; private set; }
        public string? Message { get; private set; }

        public string? StateString { get; private set; }
        public string? CodeString { get; private set; }
        public string? RedirectUri { get; private set; }

    }
}
