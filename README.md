# Webtokenexample2
In Visual Studio right mouse on Solution name and select **Set Startup Project** to run all 3 projects.

In browser open this URL to get TOKEN:

<http://localhost:40200>
<http://localhost:40201>

To test API open URL:

<http://localhost/40202/api/public>

More info:
<https://blogs.aspitalia.com/az>